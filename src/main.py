from collections import defaultdict
from enum import Enum
from typing import Dict, List

import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import OneHotEncoder


class OutcomeVariable(Enum):
    UA = 1
    REACH = 2
    FR = 3


def create_model_and_predict(training_data: pd.DataFrame, test_data: pd.DataFrame) \
        -> Dict:
    x_train = training_data.iloc[:, :-1]
    y_train = training_data.iloc[:, -1].values

    lr = LinearRegression(fit_intercept=False)
    lr.fit(x_train, y_train)

    x_test = test_data.iloc[:, :-1]
    y_test = test_data.iloc[:, -1].values

    lr = LinearRegression(fit_intercept=False)
    lr.fit(x_train, y_train)
    y_predicted = lr.predict(x_test)

    return {"model": lr, "y_test": y_test, "y_predicted": y_predicted}


def prepare_data(df_in: pd.DataFrame, categorical_variables: List[str],
                 cat_cols: Dict[str, str]) -> pd.DataFrame:
    df = df_in.copy()
    df["data_date"] = pd.to_datetime(df["data_date"])
    df["report_impressions"] = pd.to_numeric(df["report_impressions"])
    df["lb_ua"] = pd.to_numeric(df["lb_ua"])
    df["ub_ua"] = pd.to_numeric(df["ub_ua"])
    df["rd_ua"] = pd.to_numeric(df["rd_ua"])
    df["rd_fr"] = pd.to_numeric(df["rd_fr"])
    df["true_ua"] = pd.to_numeric(df["true_ua"])
    df["true_reach"] = pd.to_numeric(df["true_reach"])
    df["true_fr"] = pd.to_numeric(df["true_fr"])

    # Some campaigns have Nan as days_into_campaign, replace by mean in days_into_campaign
    df["days_into_campaign"] = df["days_into_campaign"].fillna(df["days_into_campaign"].mean())

    # Calculate the observed frequencies
    df['app_freq'] = (
            df['app_census_impressions'] / df['app_num_devices']
    ).apply(np.log10).fillna(0)
    df['browser_freq'] = (
            df['browser_census_impressions'] / df['browser_num_devices']
    ).apply(np.log10).fillna(0)

    df['log_true_reach'] = (df['true_reach']).apply(np.log10)
    df['log_true_fr'] = (df['true_fr']).apply(np.log10)
    df['log_true_ua'] = (df['true_ua']).apply(np.log10)

    ohenc = OneHotEncoder()
    enc_df = pd.DataFrame(ohenc.fit_transform(df[categorical_variables]).toarray())
    ohenc.get_feature_names()
    enc_df.columns = ohenc.get_feature_names()
    df = pd.concat([df, enc_df], axis=1)

    # Un-one-hot-encode
    for prefix, col in cat_cols.items():
        df[col] = (
                df.filter(regex=prefix).values.cumsum(axis=1) == 0).sum(axis=1)

    return df


def predict_demographics(df: pd.DataFrame, relevant_cols: List[str], train_idx: np.ndarray,
                         test_idx: np.ndarray) -> pd.DataFrame:
    # Sample training, test and create model, make predictions
    training_data = df.loc[df.index[train_idx], relevant_cols]
    test_data = df.loc[df.index[test_idx], :]
    demos = [c for c in df.columns.tolist() if c.startswith("demographic_")]
    demo_predictions = defaultdict(list)
    for demo in demos:
        demo_test_data = test_data[test_data[demo] == 1]
        demo_test_data = demo_test_data.loc[:, relevant_cols]

        results = create_model_and_predict(training_data, demo_test_data)

        y_predicted = results["y_predicted"]

        # Scaled back to frequencies
        y_predicted = 10 ** y_predicted

        # Cap predicted UA values to lower and upper bounds
        lb_ua = 10 ** demo_test_data["log_lb_ua"]
        ub_ua = 10 ** demo_test_data["log_ub_ua"]
        y_predicted = np.where(y_predicted < lb_ua, lb_ua, y_predicted)
        y_predicted = np.where(y_predicted > ub_ua, ub_ua, y_predicted)
        demo_label = demo.split("demographic_", 1)[1]

        true_ua = 10 ** demo_test_data["log_true_ua"]
        demo_predictions[demo_label].append(true_ua.values)
        rd_ua = 10 ** demo_test_data["log_rd_ua"]
        demo_predictions[demo_label].append(rd_ua.values)

        demo_predictions[demo_label].append(y_predicted)

    return demo_predictions


def create_model(df: pd.DataFrame, train_idx: np.ndarray, test_idx: np.ndarray,
                 relevant_cols: List[str]) -> Dict:
    # Sample training, test and create model, make predictions
    training_data = df.loc[df.index[train_idx], relevant_cols]
    test_data = df.loc[df.index[test_idx], relevant_cols]

    results = create_model_and_predict(training_data, test_data)

    model = results["model"]
    y_test = results["y_test"]
    y_predicted = results["y_predicted"]

    # Scaled back to frequencies
    y_test = 10 ** y_test
    y_predicted = 10 ** y_predicted

    # Cap predicted UA values to lower and upper bounds
    lb_ub = df.loc[df.index[test_idx], ["lb_ua", "ub_ua"]]
    y_predicted = np.where(y_predicted < lb_ub["lb_ua"], lb_ub["lb_ua"], y_predicted)
    y_predicted = np.where(y_predicted > lb_ub["ub_ua"], lb_ub["ub_ua"], y_predicted)

    rd_ua = 10 ** test_data["log_rd_ua"]

    return {"model": model, "true_values": y_test, "rd_ua": rd_ua, "predictions": y_predicted}


def create_model_fit_predicts(df: pd.DataFrame, train_idx: np.ndarray, test_idx: np.ndarray,
                              relevant_cols: List[str], *,
                              outcome_variable: OutcomeVariable = OutcomeVariable.UA) -> Dict:
    # Sample training, test and create model, make predictions
    training_data = df.loc[df.index[train_idx], relevant_cols]
    test_data = df.loc[df.index[test_idx], relevant_cols]

    results = create_model_and_predict(training_data, test_data)

    model = results["model"]
    y_test = results["y_test"]
    y_predicted = results["y_predicted"]

    # Scaled back to frequencies
    y_test = 10 ** y_test
    y_predicted = 10 ** y_predicted

    lb_ua = 10 ** test_data["log_lb_ua"]
    ub_ua = 10 ** test_data["log_ub_ua"]

    if outcome_variable == OutcomeVariable.REACH:
        y_test = y_test * test_data["universe_estimate"]
        y_predicted = y_predicted * test_data["universe_estimate"]

    if outcome_variable == OutcomeVariable.FR:
        diff_bounds = ub_ua - lb_ua
        y_predicted = lb_ua + diff_bounds * y_predicted
        y_test = lb_ua + diff_bounds * y_test

    # Cap predicted UA values to lower and upper bounds
    y_predicted = np.where(y_predicted < lb_ua, lb_ua, y_predicted)
    y_predicted = np.where(y_predicted > ub_ua, ub_ua, y_predicted)

    rd_ua = 10 ** test_data["log_rd_ua"]

    return {"model": model, "true_values": y_test, "rd_ua": rd_ua, "predictions": y_predicted}


