from typing import NoReturn, Tuple, List, Dict

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib.ticker import PercentFormatter
from sklearn.linear_model import LinearRegression

plt.style.use('seaborn-talk')

CB91_Blue = '#2CBDFE'
CB91_Green = '#47DBCD'
CB91_Pink = '#F3A0F2'
CB91_Purple = '#9D2EC5'
CB91_Violet = '#661D98'
CB91_Amber = '#F5B14C'

color_list = [CB91_Blue, CB91_Pink, CB91_Green, CB91_Amber,
              CB91_Purple, CB91_Violet]

plt.rcParams['axes.prop_cycle'] = plt.cycler(color=color_list)


def plot_measured_predicted(y_test: np.ndarray, y_predicted: np.ndarray,
                            title: str) -> NoReturn:
    fig, ax = plt.subplots(figsize=(9, 8))

    ax.scatter(y_test, y_predicted, c='red', edgecolors='none')
    ax.set_xlabel("Measured", fontsize=18, labelpad=10)
    ax.set_ylabel("Predicted", fontsize=18, labelpad=10)
    ax.set_title(title, fontsize=18)
    ax.set_xlabel("Measured", fontsize=18, labelpad=10)
    ax.set_ylabel("Predicted", fontsize=18, labelpad=10)
    ax.set_xticks(fontsize=18)
    ax.set_yticks(fontsize=18)
    fig.tight_layout(pad=3.0)
    plt.show()
    plt.close()


def plot_cummulated_error_plot(true_values: np.ndarray, predicted: np.ndarray,
                               x_label: str, xlim: Tuple[int, int], *,
                               n_bins: int = 100, color: str = CB91_Blue, linewidth: int = 3.0) -> NoReturn:
    """
      INPUTS:
        true_values: values to predict
        predicted: predictions
        x_label: label for x-axis
        xlim: range of values on x-axis
        n_bins: number of bins for histogram
        color: color of the cummulated plot
        linewidth: pen marker width
      OUPUT: None.
       Cummulated Relative Error Plot.
    """
    rel_err = (predicted / true_values - 1) * 100

    values, base = np.histogram(rel_err, bins=1000)
    cumulative = np.cumsum(values)

    plt.figure(figsize=(8, 4))
    plt.xlabel(x_label)
    plt.plot(base[:-1], 100 * cumulative / cumulative.max(), color=color)
    plt.xlim(xlim[0], xlim[1])
    plt.ylim([0, 100])
    plt.ylabel('Percentile')
    plt.title('Test Set Performance')
    plt.grid()
    plt.tight_layout(pad=3.0)
    plt.show()
    plt.close()


def plot_relative_errors(rel_errors: np.ndarray, \
                         x_label: str, y_label: str, *, n_bins: int = 50, color: str = "red"
                         ) -> NoReturn:
    percentages = np.sort(rel_errors * 100.0)

    fig, axes = plt.subplots(1, 3, figsize=(12, 4))
    ax0, ax1, ax2 = axes.flatten()

    ax0.hist(percentages, n_bins, density=True, histtype="step", cumulative=True, color="blue", linewidth=1.5)
    ax0.grid(True)
    ax0.set_xlabel(x_label)
    ax0.set_ylabel(y_label)

    ax0.set_xlim(-1500, 500)
    ax0.set_ylim(0, 1)
    ax0.yaxis.set_major_formatter(PercentFormatter(1))

    ax1.hist(percentages, n_bins, density=True, histtype="step", cumulative=True, color="red", linewidth=1.5)
    ax1.grid(True)
    ax1.set_xlabel(x_label)
    ax1.set_ylabel(y_label)
    ax1.set_xlim(-1500, 0)
    ax1.set_ylim(0, 0.2)
    ax1.yaxis.set_major_formatter(PercentFormatter(1))

    ax2.hist(percentages, n_bins, density=True, histtype="step", cumulative=True, color="red", linewidth=1.5)
    ax2.grid(True)
    ax2.set_xlabel(x_label)
    ax2.set_ylabel(y_label)
    ax2.set_xlim(0, 500)
    ax2.set_ylim(0.8, 1)
    ax2.yaxis.set_major_formatter(PercentFormatter(1))

    fig.tight_layout(pad=3.0)
    plt.show()
    plt.close()


def one_plot_relative_errors(rel_errors: np.ndarray, \
                             x_label: str, y_label: str, xlim: Tuple[int, int], *,
                             n_bins: int = 50, color: str = "red", linewidth: int = 3.0
                             ) -> NoReturn:
    figure, ax = plt.subplots(figsize=(8, 4))

    percentages = np.sort(rel_errors * 100)

    plt.hist(percentages, n_bins, density=True, histtype='step', cumulative=True, color=color, linewidth=linewidth)
    plt.xlim(xlim[0], xlim[1])
    plt.ylim(0, 1)
    ax.grid(True)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    plt.gca().yaxis.set_major_formatter(PercentFormatter(1))

    figure.tight_layout(pad=3.0)
    plt.show()
    plt.close()


def predict_and_plot_ua_predictions(df: pd.DataFrame, model: LinearRegression, plot_params: List[Dict]) -> NoReturn:
    x_test = df.iloc[:, :-1]
    y_test = df.iloc[:, -1].values

    y_predicted = model.predict(x_test)

    # Cap predicted UA values to lower and upper bounds
    lb_ua_test = df["lb_ua"]
    ub_ua_test = df["ub_ua"]
    y_predicted = np.where(y_predicted < lb_ua_test, lb_ua_test, y_predicted)
    y_predicted = np.where(y_predicted > ub_ua_test, ub_ua_test, y_predicted)

    rel_errors = (y_predicted / y_test) - 1
    params = plot_params[0]
    one_plot_relative_errors(rel_errors, params["x_label"], params["x_lims"], params["n_bins"], params["color"])

    rd_ua_test = df["rd_ua"]
    rel_errors = (rd_ua_test / y_test) - 1
    params = plot_params[1]
    one_plot_relative_errors(rel_errors, params["x_label"], params["y_label"], params["x_lims"], params["n_bins"],
                             params["color"])


def plot_cumulated_error_plots(true_values: np.ndarray, rd_values: np.ndarray, predicted: np.ndarray, ax, *,
                               x_label: str = "", y_label: str = "", x_lims: Tuple[int, int],
                               n_bins: int = 1000, colors: List[str] = [CB91_Blue, CB91_Green],
                               linewidth: int = 3.0) -> NoReturn:
    """
      INPUTS:
        true_values: values to predict
        rd_values: randomly generated predictions
        predicted: predictions
        ax: matplotlib axis for the plots
        x_label: x-axis label
        y_label: y-axis label
        x_lims: range of values on x-axis
        n_bins: number of bins for histogram
        colors: List of colors to use for each plot
        linewidth: pen marker width
      OUPUT: None.
       Cumulative Relative Error Plots.
    """

    ax.set_xlabel(x_label)
    rel_err = (predicted / true_values - 1) * 100
    values, base = np.histogram(rel_err, bins=n_bins)
    cumulative = np.cumsum(values)
    ax.plot(base[:-1], 100 * cumulative / cumulative.max(), color=colors[0], linewidth=linewidth, label="pred")

    rel_err = (rd_values / true_values - 1) * 100
    values, base = np.histogram(rel_err, bins=n_bins)
    cumulative = np.cumsum(values)
    ax.plot(base[:-1], 100 * cumulative / cumulative.max(), color=colors[1], linewidth=linewidth, label="rand")

    ax.set_xlim(x_lims[0], x_lims[1])
    ax.set_ylim([0, 100])
    ax.set_ylabel(y_label)
    x = np.arange(x_lims[0], x_lims[1], 5)
    ax.set_xticks(x)
    ax.legend()
    ax.grid()


def plot_UA_relative_errors(df: pd.DataFrame, suptitle: str, *, nrows: int = 4, ncols: int = 3):
    """
    Convenience function to plot cumulative error plots for UA predictions and random predictions
    """
    figure, ax = plt.subplots(nrows=nrows, ncols=ncols, sharex=False, sharey=True, figsize=(16, 20))
    figure.delaxes(ax[nrows - 1, ncols - 1])
    ax_flat = ax.flatten()

    for i in range(nrows):
        for j in range(ncols):
            index = i * ncols + j
            if index == (nrows * ncols - 1):
                break
            ax = ax_flat[index]
            demo = df.iloc[index, 0]
            true_values = df.iloc[index, 1]
            rd_values = df.iloc[index, 2]
            pred_values = df.iloc[index, 3]

            if index % ncols == 0:
                y_label = "Percentiles"
            plot_cumulated_error_plots(true_values, rd_values, pred_values, ax, x_label=demo, y_label=y_label,
                                       x_lims=(-15, 20))

    figure.tight_layout(pad=1.0)
    figure.subplots_adjust(top=0.93, hspace=0.5, wspace=0.2)
    figure.suptitle(suptitle, fontsize=18, y=0.98)

    plt.show()
    plt.close()


def plot_model_performance_charts(true_values: np.ndarray,
                                  rd_values: np.ndarray,
                                  predictions: np.ndarray,
                                  titles: List[str],
                                  colors: List[str],
                                  axes: List
                                  ) -> NoReturn:
    """
    This method plots the regression plot and cummulive error plots
    INPUTS:
      true_values: true values
      rd_values: random values
      predictions: predictions
      titles: titles of the plots
      colors: colors to use
      axes: matplotlib axes
    """

    data = pd.DataFrame({"ua": true_values, "predicted": predictions})
    sns.regplot(x="ua", y="predicted", data=data, color=colors[0], lowess=True, ax=axes[0])
    axes[0].set_title(titles[0])

    axes[1].set_title(titles[1])
    plot_cumulated_error_plots(true_values, rd_values, predictions, ax=axes[1],
                               x_label="All Countries Relative UA Errors (%)",
                               y_label="Percentiles", x_lims=(-30, 30))
