#!/usr/bin/env python3
# coding: utf-8

import copy
import datetime as dt

import pandas as pd
import numpy  as np

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader

from sklearn.model_selection import GroupShuffleSplit

from matplotlib import pyplot as plt


class TabularDataset(Dataset):

    def __init__(self, data, cat_cols=None, output_col=None):
        """
        Characterizes a Dataset for PyTorch

        Parameters
        ----------

        data: pandas data frame
          The data frame object for the input data. It must
          contain all the continuous, categorical and the
          output columns to be used.

        cat_cols: List of strings
          The names of the categorical columns in the data.
          These columns will be passed through the embedding
          layers in the model. These columns must be
          label encoded beforehand.

        output_col: string
          The name of the output variable column in the data
          provided.
        """

        self.n = data.shape[0]

        if output_col:
            self.y = data[output_col].astype(np.float32).values.reshape(-1, 1)
        else:
            self.y = np.zeros((self.n, 1))

        self.cat_cols = cat_cols if cat_cols else []
        self.cont_cols = [col for col in data.columns
                          if col not in self.cat_cols + [output_col]]

        if self.cont_cols:
            self.cont_X = data[self.cont_cols].astype(np.float32).values
        else:
            self.cont_X = np.zeros((self.n, 1))

        if self.cat_cols:
            self.cat_X = data[cat_cols].astype(np.int64).values
        else:
            self.cat_X = np.zeros((self.n, 1))

    def __len__(self):
        """
        Denotes the total number of samples.
        """
        return self.n

    def __getitem__(self, idx):
        """
        Generates one sample of data.
        """
        return [self.y[idx], self.cont_X[idx], self.cat_X[idx]]


class FeedForwardNN(nn.Module):

    def __init__(self, emb_dims, no_of_cont, lin_layer_sizes,
                 output_size, emb_dropout, lin_layer_dropouts):

        """
        Parameters
        ----------

        emb_dims: List of two element tuples
        This list will contain a two element tuple for each
        categorical feature. The first element of a tuple will
        denote the number of unique values of the categorical
        feature. The second element will denote the embedding
        dimension to be used for that feature.

        no_of_cont: Integer
        The number of continuous features in the data.

        lin_layer_sizes: List of integers.
        The size of each linear layer. The length will be equal
        to the total number
        of linear layers in the network.

        output_size: Integer
        The size of the final output.

        emb_dropout: Float
        The dropout to be used after the embedding layers.

        lin_layer_dropouts: List of floats
        The dropouts to be used after each linear layer.
        """

        super().__init__()

        # For keeping track of the device
        self.dummy_param = nn.Parameter(torch.empty(0))

        # Embedding layers
        self.emb_layers = nn.ModuleList([nn.Embedding(x, y)
                                         for x, y in emb_dims])

        no_of_embs = sum([y for x, y in emb_dims])
        self.no_of_embs = no_of_embs
        self.no_of_cont = no_of_cont

        # Linear Layers
        first_lin_layer = nn.Linear(
            self.no_of_embs + self.no_of_cont, lin_layer_sizes[0]
        )

        self.lin_layers = nn.ModuleList(
            [first_lin_layer] +
            [
                nn.Linear(lin_layer_sizes[i], lin_layer_sizes[i + 1])
                for i in range(len(lin_layer_sizes) - 1)
            ]
        )

        for lin_layer in self.lin_layers:
            nn.init.kaiming_normal_(lin_layer.weight.data)

        # Output Layer
        self.output_layer = nn.Linear(lin_layer_sizes[-1], output_size)
        nn.init.kaiming_normal_(self.output_layer.weight.data)

        # Batch Norm Layers
        self.first_bn_layer = nn.BatchNorm1d(self.no_of_cont)
        self.bn_layers = nn.ModuleList([nn.BatchNorm1d(size)
                                        for size in lin_layer_sizes])

        # Dropout Layers
        self.emb_dropout_layer = nn.Dropout(emb_dropout)
        self.droput_layers = nn.ModuleList(
            [nn.Dropout(size) for size in lin_layer_dropouts]
        )

        # Sigmoid
        self.sigmoid = nn.Sigmoid()

    def forward(self, cont_data, cat_data):

        if self.no_of_embs != 0:
            x = [
                emb_layer(cat_data[:, i])
                for i, emb_layer in enumerate(self.emb_layers)
            ]
            x = torch.cat(x, 1)
            x = self.emb_dropout_layer(x)

        if self.no_of_cont != 0:
            normalized_cont_data = self.first_bn_layer(cont_data)

        if self.no_of_embs != 0:
            x = torch.cat([x, normalized_cont_data], 1)
        else:
            x = normalized_cont_data

        for lin_layer, dropout_layer, bn_layer in \
                zip(self.lin_layers, self.droput_layers, self.bn_layers):
            x = F.relu(lin_layer(x))
            x = bn_layer(x)
            x = dropout_layer(x)

        x = self.output_layer(x)
        x = self.sigmoid(x) * 2

        return x

    def fit(self, training_data, validation_data=None,
            n_epochs=50, lr=0.05, criterion=nn.MSELoss()):

        # Get the model device
        device = self.dummy_param.device

        # Set the optimizer
        optimizer = torch.optim.Adam(model.parameters(), lr=lr)

        # Set the current best validation loss
        best_loss = np.inf

        # Get the start time
        t_start = dt.datetime.now()

        # Loop over epochs
        for epoch in range(n_epochs):

            # Ensure we are in training mode
            model.train()

            # Intitialize the training loss
            training_loss = 0
            training_batches = 0

            # Loop over batches
            for y, cont_x, cat_x in training_data:
                # Send to device
                cat_x = cat_x.to(device)
                cont_x = cont_x.to(device)
                y = y.to(device)

                # Forward Pass
                preds = model(cont_x, cat_x)
                loss = criterion(preds, y)

                # Backward Pass and Optimization
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                # Keep track of running loss
                training_loss += loss.item()
                training_batches += 1

            # Calculate training loss for the epoch
            training_loss = training_loss / training_batches

            if validation_data is not None:

                # Go into evaluation mode
                model.eval()

                # Initialize validation loss
                validation_loss = 0
                validation_batches = 0

                # Loop over batches
                for y, cont_x, cat_x in validation_data:
                    cat_x = cat_x.to(device)
                    cont_x = cont_x.to(device)
                    y = y.to(device)

                    # Forward Pass
                    preds = model(cont_x, cat_x)
                    loss = criterion(preds, y)

                    # Keep track of running loss
                    validation_loss += loss.item()
                    validation_batches += 1

                # Is this the best iteration yet?
                if validation_loss < best_loss:

                    # Store the model
                    best_model = copy.deepcopy(model)
                    best_loss = validation_loss
                    prefix = '***'

                else:
                    is_best = False
                    prefix = '   '

                # Calculate validation loss for this epoch
                validation_loss = validation_loss / validation_batches

            # Get the current time
            t_now = dt.datetime.now()

            # Print status
            if validation_data is not None:
                print(
                    f'{prefix}' +
                    f'[Epoch #{epoch+1:3d}] ' +
                    f'training loss: {training_loss:.4f}, '
                    f'validation loss: {validation_loss:.4f}, '
                    f'elapsed time: {(t_now-t_start).total_seconds():.2f}s'
                )
            else:
                print(
                    f'{prefix}' +
                    f'[Epoch #{epoch+1:3d}] ' +
                    f'training loss: {training_loss:.4f}, '
                    f'elapsed time: {(t_now-t_start).total_seconds():.2f}s'
                )

        self = best_model

    def predict(self, dataloader):

        # Initialize the outputs
        y_true = []
        y_pred = []

        # Make sure we're in eval mode
        self.eval()

        # Loop over batches
        for y, cont_x, cat_x in dataloader:
            # Store the true value
            y_true.append(y.cpu().detach().numpy())

            # Send the data to the proper device
            cat_x = cat_x.to(device)
            cont_x = cont_x.to(device)
            y = y.to(device)

            # Make the prediction
            preds = self(cont_x, cat_x)

            # Store the prediction
            y_pred.append(preds.cpu().detach().numpy())

        # Concat batches together
        y_true = np.vstack(y_true).ravel()
        y_pred = np.vstack(y_pred).ravel()

        return y_pred, y_true


# Read in the data frame
df = pd.read_csv('campaign_site_training_data.csv', index_col=0)

# Remove invalid samples
is_valid = (df['unique_audience'] > 0) & (df['impressions'] > 0)
df = df[is_valid]

# Define the categoricals to un-one-hot-encode
cat_cols = {
    'x0_': 'country',
    'x1_': 'platform_type',
    'x2_': 'category_id',
    'x3_': 'subcategory_id',
    'x4_': 'tree_level',
    # 'x5_' : 'day_of_week'
}

# Un-one-hot-encode
for prefix, col in cat_cols.items():
    df[col] = (
            df.filter(regex=prefix).values.cumsum(axis=1) == 0
    ).sum(axis=1)

# Calculate the observed frequencies
df['app_freq'] = (
        df['app_impressions_census'] / df['app_num_devices']
).apply(np.log10).fillna(0)
df['browser_freq'] = (
        df['browser_impressions_census'] / df['browser_num_devices']
).apply(np.log10).fillna(0)

# Create the object to train test split
gss = GroupShuffleSplit(n_splits=2, train_size=.7, random_state=42)

# Split, partitioning on campaign_id
for train_idx, test_idx in gss.split(df.index, df.index, df['campaign_id']):
    pass

# Define the continuous features to use
feature_cols = [
    'impressions', 'universe_estimate',
    'frequency_panel', 'cat_frequency', 'cat_proportional_frequency',
    'app_num_devices', 'app_impressions_census', 'app_freq',
    'browser_num_devices', 'browser_impressions_census', 'browser_freq',
    'unknown_num_devices', 'unknown_impressions_census',
    'app_generic_devices', 'browser_generic_devices', 'unknown_generic_devices',
    'days_into_campaign'
]

# Define the outcome features
df['log_freq'] = (df['impressions'] / df['unique_audience']).apply(np.log10)
outcome_col = 'log_freq'

df['ad_network'] = df['ad_network'].astype(int)

# Get a dataframe with each set of features
cont_features = df[feature_cols]
cat_features = df[list(cat_cols.values()) + ['ad_network']]

# Get the training an test data
relevant_cols = feature_cols + list(cat_features.columns) + [outcome_col]
training_df = df.loc[df.index[train_idx], relevant_cols]
validation_df = df.loc[df.index[test_idx], relevant_cols]

# Define the inputs used
n_vals = [cat_features[c].max() + 1 for c in cat_features.columns]
emb_dims = [(nv + 1, 2) for nv in n_vals]
no_of_cont = cont_features.shape[1]
lin_layer_sizes = [16]
output_size = 1
emb_dropout = 0.3
lin_layer_dropouts = [0.3] * len(lin_layer_sizes)

# Use the CPU
device = torch.device('cpu')

# Create the model
model = FeedForwardNN(
    emb_dims, no_of_cont, lin_layer_sizes,
    output_size, emb_dropout, lin_layer_dropouts
).to(device)

# Make a dataset for both training and validation
dataset = TabularDataset(
    data=training_df, cat_cols=list(cat_features.columns),
    output_col=outcome_col
)
val_dataset = TabularDataset(
    data=validation_df, cat_cols=list(cat_features.columns),
    output_col=outcome_col
)

# Create the batch loaders
batchsize = 4096
dataloader = DataLoader(dataset, batchsize, shuffle=True, num_workers=1)
val_dataloader = DataLoader(val_dataset, batchsize, shuffle=True, num_workers=1)

criterion = nn.MSELoss()
model.fit(
    n_epochs=100, training_data=dataloader, validation_data=val_dataloader,
    criterion=criterion, lr=0.05
)

# Make predictions
y_pred, y_true = model.predict(val_dataloader)

# Convert back to frequency
true_freq = 10 ** y_pred
pred_freq = 10 ** y_true

# Convert to unique audience
true_aud = validation_df['impressions'].values / true_freq
pred_aud = validation_df['impressions'].values / pred_freq

with plt.style.context('seaborn-talk'):
    rel_err = (pred_aud / true_aud - 1) * 100

    plt.figure(figsize=(10, 6))
    plt.hist(rel_err, bins=100)
    plt.xlabel('Relative Unique Audience Error (%)')
    plt.ylabel('Counts')
    plt.title('Validation Set Performance')
    plt.grid()
    plt.savefig('ua-relative-error-hist.png')

with plt.style.context('seaborn-talk'):
    rel_err = (pred_aud / true_aud - 1) * 100

    values, base = np.histogram(rel_err, bins=1000)
    cumulative = np.cumsum(values)

    plt.figure(figsize=(10, 6))
    plt.xlabel('Relative Unique Audience Error (%)')
    plt.plot(base[:-1], 100 * cumulative / cumulative.max())
    plt.xlim([-25, 25])
    plt.ylim([0, 100])
    plt.ylabel('Percentile')
    plt.title('Validation Set Performance')
    plt.grid()
    plt.savefig('ua-relative-error-ecdf.png')

with plt.style.context('seaborn-talk'):
    rel_err = (pred_aud / true_aud - 1) * 100

    plt.figure(figsize=(10, 6))
    plt.scatter(true_aud, pred_aud, s=2)
    plt.plot([1, 1e8], [1, 1e8], 'r:')
    plt.xlabel('True Unique Audience')
    plt.ylabel('Predicted Unique Audience')
    plt.title('Validation Set Performance')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim([100, 100e6])
    plt.ylim([100, 100e6])
    plt.grid()
    plt.savefig('ua-log-plot.png')

plt.close('all')
